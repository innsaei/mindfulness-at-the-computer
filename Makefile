# cd in Makefiles: https://stackoverflow.com/q/1789594/2525237
# General help with Makefiles: https://makefiletutorial.com/
# return (using print in python) from a python script: https://stackoverflow.com/questions/60263526/return-a-value-from-python-script-to-the-a-makefile-variable

# Each line in a recepie runs in it's own sub-shell

# Running dependencies in order:
# * Option A: target: | dep1 dep2 dep3
# * Option B: make dep1 (indented)

# If the platform is not specified (e.g. ___-windows the) commands should work on Linux
# and MacOS systems. (They may only have been tested on a Linux system though)

version:=$(shell python3 matc/constants.py)

.PHONY: clean
clean:
	rm -rf dist/ build/ mindfulness_at_the_computer.egg-info/

# .PHONY: clean-and-update
# clean-and-update:
# 	make clean
# 	git pull

.PHONY: build-pypi-test
build-pypi-test:
	make clean
	pip3 install -r requirements-dev.txt
	python3 setup.py sdist
	python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
	python3 -m webbrowser -t "https://test.pypi.org/project/mindfulness-at-the-computer/"

.PHONY: build-pypi-live
build-pypi-live:
	make clean
	pip3 install -r requirements-dev.txt
	python3 setup.py sdist
	python3 -m twine upload dist/*
	python3 -m webbrowser -t "https://pypi.org/project/mindfulness-at-the-computer/"

.PHONY: run
run:
	cd matc && python3 main.py

.PHONY: test
test:
	python3 -m unittest discover -s test
	# unittest documentation: https://docs.python.org/3/library/unittest.html

.PHONY: build-pyinstaller-linux
build-pyinstaller-linux:
	make clean
	pyinstaller mindfulness-at-the-computer-linux.spec
	cd dist && tar -czvf mindfulness-at-the-computer.tar.gz mindfulness-at-the-computer/
	mv dist/mindfulness-at-the-computer.tar.gz .
	echo ${version}
	mv -f mindfulness-at-the-computer.tar.gz mindfulness-at-the-computer_linux_${version}.tar.gz
	xdg-open build/mindfulness-at-the-computer-linux/warn-mindfulness-at-the-computer-linux.txt

# The following windows make commands have not been tested (make is difficult to run on windows)
# Instead of running make on windows we may want to simply copy and paste these commands, one
# by one.

.PHONY: ubuntu-complete-uninstall
ubuntu-complete-uninstall:
	pip3 uninstall -y mindfulness-at-the-computer
	pip3 uninstall -y PySide6
	pip3 uninstall -y shiboken6
	rm -f ~/.local/share/applications/mindfulness-at-the-computer.desktop
	rm -f ~/.config/autostart/mindfulness-at-the-computer.desktop
	rm -f ~/Desktop/mindfulness-at-the-computer.desktop


.PHONY: clean-windows
clean-windows:
	rmdir /s /q dist build mindfulness_at_the_computer.egg-info

.PHONY: build-pyinstaller-windows
build-pyinstaller-windows:
	make clean-windows
	pyinstaller mindfulness-at-the-computer-windows.spec
	# There is no easy way to create zipfiles from the windows command line, so instead we do this:
	cd dist && start .
	echo The file explorer has been opened in the dist dir. Please create the zipfile yourself.
