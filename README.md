[![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://github.com/humanetech-community/awesome-humane-tech)
[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/mindfulness-at-the-computer/community)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pipeline](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/badges/master/pipeline.svg)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/-/pipelines/latest)
[![newest release](https://img.shields.io/gitlab/v/release/mindfulness-at-the-computer/mindfulness-at-the-computer?include_prereleases)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/-/releases/)
[![PyPI downloads last week](https://img.shields.io/pypi/dw/mindfulness-at-the-computer)](https://pypistats.org/packages/mindfulness-at-the-computer)
[![SourceForge downloads last week](https://img.shields.io/sourceforge/dw/mindfulness-at-the-computer.svg)](https://sourceforge.net/projects/mindfulness-at-the-computer/files/stats/timeline)


# Mindfulness at the Computer

This application helps you to be mindful while using at the computer, and helps you concentrate on breathing in and out when you need a breathing pause.

**The [application website](https://mindfulness-at-the-computer.gitlab.io) has more information** (including screenshots and downloads).

Social: [**Gitter chat**](https://gitter.im/mindfulness-at-the-computer/community)

License: GPLv3 - a *free software* (free as in freedom) license, meaning that you can share, change and distribute the application

The application is written in Python and uses Qt/PySide for the front-end.

The project has been featured in the [PyCoders Weekly](https://pycoders.com/) newsletter and is listed in [Awesome Humane Tech](https://github.com/humanetech-community/awesome-humane-tech)

## Installation and setup

Install with pip `pip install mindfulness-at-the-computer` or [**download the pre-built package**](https://sourceforge.net/projects/mindfulness-at-the-computer/files/latest/download) from sourceforge. (For details about installation and setup, please see [the website](https://mindfulness-at-the-computer.gitlab.io/installation/))

## What is Mindfulness?

https://mindfulness-at-the-computer.gitlab.io/mindfulness/


# Project

Project goals:
* Providing a friendly environment for people new to software and development to contribute and improve their skills
* Promoting [free/libre software](https://www.gnu.org/philosophy/free-sw.en.html) (free as in freedom!) - and helping people see free software in the context of human rights

Software goals:
* Increasing mindfulness of breathing, body, and posture
* Helping people become more aware of (and kind to) their bodies when using the computer for long periods of time


# Contributing to the project

*This project is beginner-friendly:* You can ask for help in the Gitter chat room and we will try to help you. Also we try to provide documentation useful for newcomers.

## Motivation when contributing to this project

> *"Don't do anything except with the joy of a small child feeding a hungry duck"*

By feeding the hungry duck, you are helping their well-being and making them happy. You’re not feeding it just because you feel like you *should* or *ought* to, but because it makes both of you happy. That’s how we’d love for you to help with this project! When you contribute, not only are you are helping this project, you are helping others who will use this application, helping them to be mindful whilst working at the computer and to be aware of their breathing and well-being. 

## Developer documentation

Most documents are located in the [docs directory](docs). There's also a [wiki](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/wikis/home) that is open for editing

Some important documents:
* [**CONTRIBUTING**](CONTRIBUTING.md)
* [Technical Architecture](docs/tech-architecture.md)
* [Running the application from source](docs/howto/running-from-source.md)

[Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/) is used for the text documents. [draw.io](https://www.draw.io/) is used for editing the `.xml` files.
