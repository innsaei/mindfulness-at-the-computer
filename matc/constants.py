"""
This file is separete from shared.py since that file/module contains functions that require access to QtCore
and QtGui, and this file has to be used during the setup process, before we have imported pyside
"""

APPLICATION_VERSION = "1.0.0-alpha.11"
# [Semantic versioning](semver.org) is used, and we may also add `dev` at the end of the version
# Example: `1.2.3-alpha.4-dev.5`

APPLICATION_PRETTY_NAME = "Mindfulness at the Computer"
APPLICATION_NAME = "mindfulness-at-the-computer"
SHORT_DESCR_STR = "Helps you stay mindful of your breathing while using your computer."

BREATHING_PHRASE_NOT_SET: int = -1


# The following is used in the makefile for automatically naming the archive file:
if __name__ == "__main__":
    print(APPLICATION_VERSION)
