import logging
import os
import sys
from PySide6 import QtCore
from PySide6 import QtGui
from PySide6 import QtWidgets
import matc.constants
import matc.shared
import matc.settings
import matc.gui.breathing_dlg

NEW_ROW: int = -1


class SettingsDlg(QtWidgets.QDialog):
    br_timer_change_signal = QtCore.Signal()
    notif_audio_test_signal = QtCore.Signal()
    br_audio_test_signal = QtCore.Signal()

    def __init__(self):
        super().__init__()
        self.setGeometry(100, 64, 400, 540)
        self.setWindowTitle(f"Settings Dialog - {matc.constants.APPLICATION_PRETTY_NAME}")
        self.setWindowIcon(QtGui.QIcon(matc.shared.get_app_icon_path("icon.png")))

        self.updating_gui: bool = True

        """
        if matc.shared.testing_bool:
            data_storage_str = "{Testing - data stored in memory}"
        else:
            data_storage_str = "{Live - data stored on hard drive}"
        window_title_str = (
                matc.constants.APPLICATION_PRETTY_NAME
                + " [" + matc.constants.APPLICATION_VERSION + "] "
                + data_storage_str
        )
        self.setWindowTitle(window_title_str)
        """

        self.setStyleSheet(
            "selection-background-color:" + matc.shared.LIGHT_GREEN_COLOR + ";"
            "selection-color:#000000;"
        )

        hbox_l2 = QtWidgets.QHBoxLayout()
        self.setLayout(hbox_l2)

        vbox_l2 = QtWidgets.QVBoxLayout()
        hbox_l2.addLayout(vbox_l2)

        hbox_br_time_l3 = QtWidgets.QHBoxLayout()
        vbox_l2.addLayout(hbox_br_time_l3)
        hbox_br_time_l3.addWidget(QtWidgets.QLabel("Breathing break time"))
        self.breathing_break_time_qsb = QtWidgets.QSpinBox()
        self.breathing_break_time_qsb.setMinimum(1)
        self.breathing_break_time_qsb.setMaximum(1000)
        hbox_br_time_l3.addWidget(self.breathing_break_time_qsb)
        self.breathing_break_time_qsb.valueChanged.connect(self.on_br_time_value_changed)
        hbox_br_time_l3.addWidget(QtWidgets.QLabel("minutes"))

        self.phrases_qgb = QtWidgets.QGroupBox("Breathing phrases")
        vbox_l2.addWidget(self.phrases_qgb)
        vbox_l4 = QtWidgets.QVBoxLayout()
        self.phrases_qgb.setLayout(vbox_l4)

        self.edit_bp_qpb = QtWidgets.QPushButton("Edit")

        self.breathing_phrases_qlw = MyListWidget()
        vbox_l4.addWidget(self.breathing_phrases_qlw)
        self.breathing_phrases_qlw.setSpacing(2)
        self.breathing_phrases_qlw.currentRowChanged.connect(self.on_bp_current_row_changed)
        # self.breathing_phrases_qlw.itemDoubleClicked.connect(self.on_bp_double_clicked)
        self.breathing_phrases_qlw.drop_signal.connect(self.on_bp_item_dropped)
        self.populate_bp_list()

        hbox_buttons_l5 = QtWidgets.QHBoxLayout()
        vbox_l4.addLayout(hbox_buttons_l5)

        hbox_buttons_l5.addWidget(self.edit_bp_qpb)
        self.edit_bp_qpb.clicked.connect(self.on_edit_bp_clicked)

        self.add_bp_qpb = QtWidgets.QPushButton("Add")
        hbox_buttons_l5.addWidget(self.add_bp_qpb)
        self.add_bp_qpb.clicked.connect(self.on_add_bp_clicked)

        self.del_bp_qpb = QtWidgets.QPushButton("Del")
        hbox_buttons_l5.addWidget(self.del_bp_qpb)
        self.del_bp_qpb.clicked.connect(self.on_del_bp_clicked)

        """
        self.set_active_bp_qpb = QtWidgets.QPushButton("Set Active")
        hbox_buttons_l5.addWidget(self.set_active_bp_qpb)
        self.set_active_bp_qpb.clicked.connect(self.on_set_active_bp_clicked)
        """

        self.visualizations_qgb = QtWidgets.QGroupBox("Breathing visualizations")
        hbox_l2.addWidget(self.visualizations_qgb)
        grid_l4 = QtWidgets.QGridLayout()
        self.visualizations_qgb.setLayout(grid_l4)
        self.br_vis_group_qbg = QtWidgets.QButtonGroup()

        self.bar_bv_option_cw=BrVisOptionCw(matc.shared.BreathingVisalization.bar)
        grid_l4.addWidget(self.bar_bv_option_cw, 0, 0)
        self.br_vis_group_qbg.addButton(
            self.bar_bv_option_cw.br_vis_qrb,
            matc.shared.BreathingVisalization.bar.value
        )

        self.circle_bv_option_cw=BrVisOptionCw(matc.shared.BreathingVisalization.circle)
        grid_l4.addWidget(self.circle_bv_option_cw, 0, 1)
        self.br_vis_group_qbg.addButton(
            self.circle_bv_option_cw.br_vis_qrb,
            matc.shared.BreathingVisalization.circle.value
        )

        self.line_bv_option_cw=BrVisOptionCw(matc.shared.BreathingVisalization.line)
        grid_l4.addWidget(self.line_bv_option_cw, 1, 0)
        self.br_vis_group_qbg.addButton(
            self.line_bv_option_cw.br_vis_qrb,
            matc.shared.BreathingVisalization.line.value
        )

        self.columns_bv_option_cw=BrVisOptionCw(matc.shared.BreathingVisalization.columns)
        grid_l4.addWidget(self.columns_bv_option_cw, 1, 1)
        self.br_vis_group_qbg.addButton(
            self.columns_bv_option_cw.br_vis_qrb,
            matc.shared.BreathingVisalization.columns.value
        )

        """
        self.br_vis_bar_qrb = QtWidgets.QRadioButton("Bar")
        self.br_vis_group_qbg.addButton(self.br_vis_bar_qrb, matc.shared.BreathingVisalization.bar.value)
        vbox_l4.addWidget(self.br_vis_bar_qrb)
        self.bar_preview_qll = QtWidgets.QLabel()
        self.bar_preview_qll.setPixmap(preview_bm_dict[matc.shared.BreathingVisalization.bar.value])
        vbox_l4.addWidget(self.bar_preview_qll)

        self.br_vis_circle_qrb = QtWidgets.QRadioButton("Circle")
        self.br_vis_group_qbg.addButton(self.br_vis_circle_qrb, matc.shared.BreathingVisalization.circle.value)
        vbox_l4.addWidget(self.br_vis_circle_qrb)

        self.br_vis_line_qrb = QtWidgets.QRadioButton("Line")
        self.br_vis_group_qbg.addButton(self.br_vis_line_qrb, matc.shared.BreathingVisalization.line.value)
        vbox_l4.addWidget(self.br_vis_line_qrb)

        self.br_vis_cols_qrb = QtWidgets.QRadioButton("Columns (includes history)")
        self.br_vis_group_qbg.addButton(self.br_vis_cols_qrb, matc.shared.BreathingVisalization.columns.value)
        vbox_l4.addWidget(self.br_vis_cols_qrb)
        """
        self.br_vis_group_qbg.idClicked.connect(self.on_br_vis_id_clicked)


        self.move_mouse_cursor_qcb = QtWidgets.QCheckBox("Move mouse cursor to breathing dialog")
        vbox_l2.addWidget(self.move_mouse_cursor_qcb)
        self.move_mouse_cursor_qcb.clicked.connect(self.on_move_mouse_cursor_clicked)
        self.move_mouse_cursor_qcb.setToolTip("Convenient if you are using a touchpad")

        hbox_volume_l3 = QtWidgets.QHBoxLayout()
        vbox_l2.addLayout(hbox_volume_l3)
        hbox_volume_l3.addWidget(QtWidgets.QLabel("Master volume"))
        self.volume_qsr = QtWidgets.QSlider()
        hbox_volume_l3.addWidget(self.volume_qsr)
        self.volume_qsr.valueChanged.connect(self.on_volume_changed)
        self.volume_qsr.setMinimum(0)
        self.volume_qsr.setMaximum(100)
        self.volume_qsr.setOrientation(QtCore.Qt.Horizontal)

        self.notification_audio_cw = AudioCw("Notification",
            matc.settings.SK_NOTIFICATION_AUDIO_FILE_PATH, matc.settings.SK_NOTIFICATION_AUDIO_VOLUME)
        vbox_l2.addWidget(self.notification_audio_cw)
        self.notification_audio_cw.test_clicked_signal.connect(self.notif_audio_test_signal.emit)

        self.breathing_audio_cw = AudioCw("Breathing",
            matc.settings.SK_BREATHING_AUDIO_FILE_PATH, matc.settings.SK_BREATHING_AUDIO_VOLUME)
        vbox_l2.addWidget(self.breathing_audio_cw)
        self.breathing_audio_cw.test_clicked_signal.connect(self.br_audio_test_signal.emit)

        self.update_gui()

    def populate_bp_list(self):
        self.breathing_phrases_qlw.clear()
        phrases: list[matc.settings.BreathingPhrase] = matc.settings.settings[matc.settings.SK_BREATHING_PHRASES]
        for p in phrases:
            self._add_bp_to_gui(p.id)

    def on_bp_item_dropped(self):
        new_order_phrase_list = []
        for item_row in range(self.breathing_phrases_qlw.count()):
            item = self.breathing_phrases_qlw.item(item_row)
            item_id = item.data(QtCore.Qt.UserRole)
            phrase = matc.settings.get_breathing_phrase(item_id)
            new_order_phrase_list.append(phrase)
        matc.settings.settings[matc.settings.SK_BREATHING_PHRASES] = new_order_phrase_list
        # self.populate_bp_list()

    def on_bp_double_clicked(self, i_item: QtWidgets.QListWidgetItem):
        matc.shared.active_phrase_id = i_item.data(QtCore.Qt.UserRole)
        self.update_gui_active_bold()

    def on_move_mouse_cursor_clicked(self, i_checked: bool):
        if self.updating_gui:
            return
        matc.settings.settings[matc.settings.SK_MOVE_MOUSE_CURSOR] = i_checked

    def on_br_vis_id_clicked(self, i_id: int):
        if self.updating_gui:
            return
        matc.settings.settings[matc.settings.SK_BREATHING_VISUALIZATION] = i_id

    def on_bp_current_row_changed(self):
        pass

    """
    def on_show_br_phrase_toggled(self, i_checked: bool):
        logging.debug("on_show_br_phrase_toggled")
        if i_checked:
            top_qlwi = self.breathing_phrases_qlw.item(0)
            matc.shared.active_phrase_id = top_qlwi.data(QtCore.Qt.UserRole)
        else:
            matc.shared.active_phrase_id = matc.shared.BREATHING_PHRASE_NOT_SET
        self.update_gui_active_bold()

    def on_set_active_bp_clicked(self):
        current_qlwi = self.breathing_phrases_qlw.currentItem()
        matc.shared.active_phrase_id = current_qlwi.data(QtCore.Qt.UserRole)
        self.update_gui_active_bold()

    def update_gui_active_bold(self):
        for item_row in range(self.breathing_phrases_qlw.count()):
            item = self.breathing_phrases_qlw.item(item_row)
            item_font = item.font()
            if item.data(QtCore.Qt.UserRole) == matc.shared.active_phrase_id:
                item_font.setBold(True)
                item.setFont(item_font)
            elif item_font.bold():
                item_font.setBold(False)
                item.setFont(item_font)

            self.show_breathing_phrase_qcb.setChecked(
                matc.shared.active_phrase_id != matc.shared.BREATHING_PHRASE_NOT_SHOWN
            )
    """

    def on_volume_changed(self, i_new_value: int):
        if self.updating_gui:
            return
        matc.settings.settings[matc.settings.SK_MASTER_VOLUME] = i_new_value

    def on_add_bp_clicked(self):
        new_id: int = matc.settings.add_breathing_phrase("ib", "ob")
        result = BreathingPhraseEditDialog.start(new_id)
        if result:
            self._add_bp_to_gui(new_id)

    def _update_bp_in_gui(self, i_id: int, i_row: int):
        phrase = matc.settings.get_breathing_phrase(i_id)
        phrase_text: str = f"{phrase.in_breath}\n{phrase.out_breath}"

        qlwi = QtWidgets.QListWidgetItem(phrase_text)
        self.breathing_phrases_qlw.takeItem(i_row)
        self.breathing_phrases_qlw.insertItem(i_row, qlwi)
        self.breathing_phrases_qlw.setCurrentRow(i_row)

    def _add_bp_to_gui(self, i_id: int, i_row: int = NEW_ROW):
        """
        if i_id == matc.constants.BREATHING_PHRASE_NOT_SET:
            new_font = qlwi.font()
            new_font.setItalic(True)
            qlwi.setFont(new_font)
        if i_id == matc.constants.BREATHING_PHRASE_NOT_SET:
            # phrase_text: str = "nothing"
            raise Exception("BREATHING_PHRASE_NOT_SET --- this should not be possible")
        else:
        """
        phrase = matc.settings.get_breathing_phrase(i_id)
        phrase_text: str = f"{phrase.in_breath}\n{phrase.out_breath}"
        qlwi = QtWidgets.QListWidgetItem(phrase_text)
        qlwi.setData(QtCore.Qt.UserRole, i_id)
        if i_row == NEW_ROW:
            self.breathing_phrases_qlw.addItem(qlwi)
            new_row: int = self.breathing_phrases_qlw.count() - 1
            self.breathing_phrases_qlw.setCurrentRow(new_row)
        else:
            self.breathing_phrases_qlw.insertItem(i_row, qlwi)
            self.breathing_phrases_qlw.setCurrentRow(i_row)

    def on_del_bp_clicked(self):
        if self.breathing_phrases_qlw.count() == 0:
            QtWidgets.QMessageBox.information(self, "Cannot remove last item",
                                              "It's not possible to remove the last item")
            return

        current_item = self.breathing_phrases_qlw.currentItem()
        current_row: int = self.breathing_phrases_qlw.currentRow()
        id_: int = current_item.data(QtCore.Qt.UserRole)

        if matc.shared.active_phrase_id == id_:
            QtWidgets.QMessageBox.information(self, "Cannot remove active item",
                                              "It's not possible to remove the active item. Please switch to another item before removing this one")
            return

        standard_button = QtWidgets.QMessageBox.question(self, "Removing br phrase",
                                                         f"Are you sure you want to remove this item:\n\n{current_item.text()}")
        if standard_button == QtWidgets.QMessageBox.Yes:
            matc.settings.remove_breathing_phrase(id_)
            self.breathing_phrases_qlw.takeItem(current_row)

    def on_edit_bp_clicked(self):
        # bp_edit_dlg = BreathingPhraseEditDialog()
        # bp_edit_dlg.start()
        current_item = self.breathing_phrases_qlw.currentItem()
        current_row: int = self.breathing_phrases_qlw.currentRow()
        id_: int = current_item.data(QtCore.Qt.UserRole)

        result = BreathingPhraseEditDialog.start(id_)
        if result:
            self._update_bp_in_gui(id_, current_row)

        # qlwi = self.breathing_phrases_qlw.takeItem(current_row)
        # self._add_bp_to_gui(id_, current_row)
        # matc.shared.active_phrase_id_it

    def on_br_time_value_changed(self, i_new_value: int):
        if self.updating_gui:
            return
        matc.settings.settings[matc.settings.SK_BREATHING_BREAK_TIMER_SECS] = 60 * i_new_value
        self.br_timer_change_signal.emit()

    def update_menu(self):
        self.menu_bar.clear()

        """
        file_menu = self.menu_bar.addMenu("&File")

        self.update_gui_action = QtGui.QAction("Update settings GUI", self)
        self.debug_menu.addAction(self.update_gui_action)
        self.update_gui_action.triggered.connect(self.update_gui)

        minimize_to_tray_action = QtGui.QAction(self.tr("Minimize to tray"), self)
        file_menu.addAction(minimize_to_tray_action)
        minimize_to_tray_action.triggered.connect(self.minimize_to_tray)
        save_action = QtGui.QAction(self.tr("Save"), self)
        file_menu.addAction(save_action)
        save_action.triggered.connect(matc.settings.save_settings_to_json_file)
        choose_file_directory_action = QtGui.QAction(self.tr("Choose file directory"), self)
        file_menu.addAction(choose_file_directory_action)
        choose_file_directory_action.triggered.connect(pass)
        quit_action = QtGui.QAction("Quit application", self)
        file_menu.addAction(quit_action)
        quit_action.triggered.connect(self.exit_application)
        close_settings_action = QtGui.QAction("Close settings", self)
        file_menu.addAction(close_settings_action)
        close_settings_action.triggered.connect(self.close)
        """

    def on_breathing_notification_breathe_clicked(self):
        self.open_breathing_dialog(i_mute_override=True)

    def debug_clear_breathing_phrase_selection(self):
        self.br_phrase_list_wt.list_widget.clearSelection()

    def show_online_help(self):
        url_str = "https://mindfulness-at-the-computer.gitlab.io/user_guide"
        # noinspection PyCallByClass
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url_str))
        # Python: webbrowser.get(url_str) --- doesn't work

    # noinspection PyPep8Naming
    def closeEvent(self, i_QCloseEvent):
        matc.settings.save_settings_to_json_file()
        super().closeEvent(i_QCloseEvent)

    def exit_application(self):
        QtWidgets.QApplication.quit()
        # sys.exit()

    def update_gui(self):
        self.updating_gui = True

        br_time_value: int = matc.settings.settings[matc.settings.SK_BREATHING_BREAK_TIMER_SECS]
        self.breathing_break_time_qsb.setValue(br_time_value // 60)

        volume: int = matc.settings.settings[matc.settings.SK_MASTER_VOLUME]
        self.volume_qsr.setValue(volume)

        # self.update_gui_active_bold()

        active_br_vis_id: int = matc.settings.settings[matc.settings.SK_BREATHING_VISUALIZATION]
        for btn in self.br_vis_group_qbg.buttons():
            if self.br_vis_group_qbg.id(btn) == active_br_vis_id:
                # self.updating_gui = False
                btn.click()
                # self.updating_gui = True

        move_mouse_cursor: bool = matc.settings.settings[matc.settings.SK_MOVE_MOUSE_CURSOR]
        self.move_mouse_cursor_qcb.setChecked(move_mouse_cursor)

        self.updating_gui = False


class PreviewLabel(QtWidgets.QLabel):
    mouse_press_signal = QtCore.Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        self.mouse_press_signal.emit()



class ToggleSwitchWt(QtWidgets.QWidget):
    toggled_signal = QtCore.Signal(bool)

    def __init__(self):
        super().__init__()

        self.updating_gui_bool = False

        self.turn_on_off_qcb = QtWidgets.QCheckBox()
        self.turn_on_off_qcb.toggled.connect(self._on_toggled)
        on_off_qhl = QtWidgets.QHBoxLayout()
        on_off_qhl.setContentsMargins(0, 0, 0, 0)
        on_off_qhl.addWidget(QtWidgets.QLabel(self.tr("Turn the dialog and notifications on or off")))
        on_off_qhl.addStretch(1)
        on_off_qhl.addWidget(self.turn_on_off_qcb)
        self.setLayout(on_off_qhl)

    def _on_toggled(self, i_checked: bool):
        if self.updating_gui_bool:
            return
        self.toggled_signal.emit(i_checked)

    def update_gui(self, i_checked: bool):
        self.updating_gui_bool = True

        self.turn_on_off_qcb.setChecked(i_checked)

        self.updating_gui_bool = False


class BreathingPhraseEditDialog(QtWidgets.QDialog):
    def __init__(self, i_id: int, i_parent=None):
        super().__init__(parent=i_parent)
        self.setModal(True)
        self.setMinimumWidth(250)
        vbox = QtWidgets.QVBoxLayout(self)

        """
        # If a phrase is not selected, default to phrase with id 1
        if matc.shared.active_phrase_id_it == matc.shared.NO_PHRASE_SELECTED_INT:
            matc.shared.active_phrase_id_it = 1
        """

        bp_obj = matc.settings.get_breathing_phrase(i_id)

        self.breath_title_qle = QtWidgets.QLineEdit(str(bp_obj.id))
        vbox.addWidget(QtWidgets.QLabel(self.tr("ID")))
        vbox.addWidget(self.breath_title_qle)

        vbox.addWidget(QtWidgets.QLabel(self.tr("Phrase")))
        self.in_breath_phrase_qle = QtWidgets.QLineEdit(bp_obj.in_breath)
        vbox.addWidget(self.in_breath_phrase_qle)

        self.out_breath_phrase_qle = QtWidgets.QLineEdit(bp_obj.out_breath)
        vbox.addWidget(self.out_breath_phrase_qle)

        self.button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel,
            QtCore.Qt.Horizontal,
            self
        )
        vbox.addWidget(self.button_box)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        # -accept and reject are "slots" built into Qt

        self.adjustSize()

    @staticmethod
    def start(i_id) -> bool:
        dlg = BreathingPhraseEditDialog(i_id)
        dlg.exec()

        if dlg.result() == QtWidgets.QDialog.Accepted:
            logging.debug("dlg.result() == QtWidgets.QDialog.Accepted")
            bp_obj = matc.settings.get_breathing_phrase(i_id)
            bp_obj.in_breath = dlg.in_breath_phrase_qle.text()
            bp_obj.out_breath = dlg.out_breath_phrase_qle.text()
            return True
        return False


class MyListWidget(QtWidgets.QListWidget):
    drop_signal = QtCore.Signal()

    def __init__(self):
        super().__init__()
        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

    def dropEvent(self, QDropEvent):
        super().dropEvent(QDropEvent)
        self.drop_signal.emit()
        # self.update_db_sort_order_for_all_rows()


class BrVisOptionCw(QtWidgets.QWidget):
    def __init__(self, item: matc.shared.BreathingVisalization):
        super().__init__()
        self.vbox = QtWidgets.QVBoxLayout()
        self.setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        self.setLayout(self.vbox)
        self.br_vis_qrb = QtWidgets.QRadioButton(item.name.capitalize())
        new_font = self.br_vis_qrb.font()
        new_font.setPointSize(new_font.pointSize() + 2)
        self.br_vis_qrb.setFont(new_font)
        self.preview_qll = PreviewLabel()
        pixmap = matc.gui.breathing_dlg.BreathingGraphicsView.get_preview_pixmap(item)
        self.preview_qll.setPixmap(pixmap)
        self.preview_qll.mouse_press_signal.connect(self.br_vis_qrb.click)
        self.vbox.addWidget(self.br_vis_qrb)
        self.vbox.addWidget(self.preview_qll)


class AudioCw(QtWidgets.QWidget):
    test_clicked_signal = QtCore.Signal()

    def __init__(self, i_title: str, i_file_path_settings_key: str, i_volume_settings_key: str):
        super().__init__()
        self.file_path_settings_key = i_file_path_settings_key
        self.volume_settings_key = i_volume_settings_key
        self.updating_gui: bool = True

        vbox_l2 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l2)

        hbox = QtWidgets.QHBoxLayout()
        vbox_l2.addLayout(hbox)

        self.title_qll = QtWidgets.QLabel(i_title)
        hbox.addWidget(self.title_qll)
        new_font = self.title_qll.font()
        new_font.setBold(True)
        new_font.setPointSize(new_font.pointSize()+1)
        self.title_qll.setFont(new_font)
        hbox.addStretch(1)
        self.test_qpb = QtWidgets.QPushButton("Test")
        hbox.addWidget(self.test_qpb)
        self.test_qpb.clicked.connect(self.test_clicked_signal.emit)

        hbox = QtWidgets.QHBoxLayout()
        vbox_l2.addLayout(hbox)
        self.file_name_qll = QtWidgets.QLabel("File:")
        hbox.addWidget(self.file_name_qll)
        self.file_qcb = QtWidgets.QComboBox()
        hbox.addWidget(self.file_qcb)
        self.file_qcb.currentIndexChanged.connect(self.on_file_index_changed)
        # self.file_qcb.clicked.connect(self.on_change_file_clicked)
        """
        self.change_file_qpb = QtWidgets.QPushButton("Change")
        hbox.addWidget(self.change_file_qpb)
        self.change_file_qpb.clicked.connect(self.on_change_file_clicked)
        """

        hbox = QtWidgets.QHBoxLayout()
        vbox_l2.addLayout(hbox)
        self.volume_qll = QtWidgets.QLabel("Volume")
        hbox.addWidget(self.volume_qll)
        self.volume_qsr = QtWidgets.QSlider()
        hbox.addWidget(self.volume_qsr)
        self.volume_qsr.setOrientation(QtCore.Qt.Horizontal)
        self.volume_qsr.setRange(0, 100)
        self.volume_qsr.setValue(50)
        self.volume_qsr.valueChanged.connect(self.on_volume_value_changed)

        self.update_gui()
        self.updating_gui: bool = False

    def on_volume_value_changed(self, i_new_value: int):
        if self.updating_gui:
            return
        logging.debug(f"{i_new_value=}")
        matc.settings.settings[self.volume_settings_key] = i_new_value

    def on_file_index_changed(self, i_new_index: int):
        if self.updating_gui:
            return
        audio_file_path: str = self.file_qcb.itemData(i_new_index)
        logging.debug(f"{audio_file_path=}")
        matc.settings.settings[self.file_path_settings_key] = audio_file_path

    def update_gui(self):
        self.updating_gui = True

        active_audio_path: str = matc.settings.settings[self.file_path_settings_key]

        def add_files_from_dir(i_dir_path) -> int:
            audio_dir_items = [x for x in os.listdir(i_dir_path) if x.endswith(".wav")]
            for audio_fn in sorted(audio_dir_items):
                audio_path = os.path.join(i_dir_path, audio_fn)
                self.file_qcb.addItem(audio_fn, userData=audio_path)
                if audio_path == active_audio_path:
                    index_of_last: int = self.file_qcb.count()-1
                    self.file_qcb.setCurrentIndex(index_of_last)
            return len(audio_dir_items)

        res_audio_dir = matc.shared.get_audio_path()
        nr_added_res_audio = add_files_from_dir(res_audio_dir)
        config_dir = matc.shared.get_config_path()
        nr_added_config = add_files_from_dir(config_dir)
        if nr_added_config > 0:
            self.file_qcb.insertSeparator(nr_added_res_audio)

        volume: int = matc.settings.settings[self.volume_settings_key]
        self.volume_qsr.setValue(volume)

        self.updating_gui = False

        """
        res_audio_dir = matc.shared.get_audio_path()
        res_audio_dir_items: list = get_wav_files(res_audio_dir)
        for res_audio_fn in res_audio_dir_items:
            res_audio_path = os.path.join(res_audio_dir, res_audio_fn)
            self.file_qcb.addItem(text=res_audio_fn, userData=res_audio_path)
        # self.file_qcb.addItems(res_audio_dir_items)
        
        config_dir_items: list = get_wav_files(matc.shared.get_config_path())
        self.file_qcb.addItems(config_dir_items)
        """


if __name__ == "__main__":
    matc_qapplication = QtWidgets.QApplication(sys.argv)
    win = SettingsDlg()
    win.show()
    sys.exit(matc_qapplication.exec_())
