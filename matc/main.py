import logging
import logging.handlers
import faulthandler
import os
import sys
import argparse
import trace

from PySide6 import QtCore
from PySide6 import QtWidgets
import matc.constants
import matc.shared
import matc.settings
import matc.main_object

# The following import looks like it isn't used, but it is necessary for importing the images.
import matc.matc_rc  # pylint: disable=unused-import

LOG_FILE_NAME_STR = "matc.log"
JSON_SETTINGS_FILE_NAME = "settings.json"


def on_about_to_quit():
    logging.debug("on_about_to_quit --- saving settings to json file (in the user config dir)")
    matc.settings.save_settings_to_json_file()
    logging.info("Exiting application")


def main():
    # db_filepath: str = matc.shared.get_database_path()
    # matc.shared.db_file_exists_at_application_startup_bl = os.path.isfile(db_filepath)
    # -settings this variable before the file has been created

    logger = logging.getLogger()
    # -if we set a name here for the logger the file handler will no longer work, unknown why
    logger.handlers = []  # -removing the default stream handler first
    # logger.propagate = False
    log_path_str = matc.shared.get_config_path(LOG_FILE_NAME_STR)
    rfile_handler = logging.handlers.RotatingFileHandler(log_path_str, maxBytes=8192, backupCount=2)
    rfile_handler.setLevel(logging.INFO)
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    rfile_handler.setFormatter(formatter)
    logger.addHandler(rfile_handler)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    if sys.flags.dev_mode:
        # logging.debug(f"{faulthandler.is_enabled()=}")
        # fh_file_path=matc.shared.get_config_path("filehandler_output.log")
        log_file_fd=open(log_path_str, "a")
        faulthandler.enable(log_file_fd)
        """
        > By default, the Python traceback is written to sys.stderr. To see tracebacks, applications must be run in the terminal. A log file can alternatively be passed to faulthandler.enable().
        https://docs.python.org/3/library/faulthandler.html    
        """

    # Handling of (otherwise) uncaught exceptions
    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            # -"KeyboardInterrupt" includes ctrl+c and exiting the application with the "stop" button in PyCharm
            logging.debug("KeyboardInterrupt")
        else:
            logging.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
        """
        if issubclass(exc_type, KeyboardInterrupt):
            sys.excepthook(exc_type, exc_value, exc_traceback)
        if issubclass(exc_type, Exception):
            logger.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
        else:
            sys.excepthook(exc_type, exc_value, exc_traceback)
        """
    sys.excepthook = handle_exception
    # -can be tested with: raise RuntimeError("RuntimeError")

    def get_settings_file_path(i_date_text: str = ""):
        config_path = matc.shared.get_config_path()
        json_file_name: str = JSON_SETTINGS_FILE_NAME
        if i_date_text:
            json_file_name = json_file_name + "_" + i_date_text
        json_file_path = os.path.join(config_path, json_file_name)
        return json_file_path

    default_settings_file_path = get_settings_file_path()

    argument_parser = argparse.ArgumentParser(prog=matc.constants.APPLICATION_PRETTY_NAME,
        description=matc.constants.SHORT_DESCR_STR)
    argument_parser.add_argument("--settings-file", "-s", default=default_settings_file_path,
        help="Path to a settings file (json format). If only a file name is given, the default directory will be used")
    parsed_args = argument_parser.parse_args()
    dir_ = os.path.dirname(parsed_args.settings_file)
    if dir_ and os.path.isdir(dir_):
        matc.settings.settings_file_path = parsed_args.settings_file
    else:
        default_dir = os.path.dirname(default_settings_file_path)
        matc.settings.settings_file_path = os.path.join(default_dir, parsed_args.settings_file)
    # matc.settings.update_settings_dict_with_json_data(matc.settings.settings, matc.shared.settings_file_path)
    matc.settings.update_settings_dict_with_json_data()

    matc_qapplication = QtWidgets.QApplication(sys.argv)

    logging.info("Starting application")

    # Application information
    sys_info_telist=matc.shared.get_system_info()
    logging.debug("##### System Information #####")
    for (descr_str, value) in sys_info_telist:
        logging.debug(descr_str + ": " + str(value))
    logging.debug("#####")

    # set stylesheet
    stream = QtCore.QFile(os.path.join(matc.shared.get_module_path(), "matc.qss"))
    stream.open(QtCore.QIODevice.ReadOnly)
    matc_qapplication.setStyleSheet(QtCore.QTextStream(stream).readAll())

    matc_qapplication.setQuitOnLastWindowClosed(False)
    matc_qapplication.aboutToQuit.connect(on_about_to_quit)

    is_first_time_started = False
    if not matc.settings.settings_file_exists():
        # if not matc.shared.testing_bool:
        is_first_time_started = True


    print(f"{sys.flags.dev_mode=}")
    # print(f"{matc.shared.testing_bool=}")


    matc_main_object = matc.main_object.MainObject()
    matc_main_object.initialize(is_first_time_started)
    sys.exit(matc_qapplication.exec())


if __name__ == "__main__":
    main()
