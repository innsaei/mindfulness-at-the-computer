import os.path
import sys
import logging
import webbrowser
from PySide6 import QtCore
from PySide6 import QtGui
from PySide6 import QtWidgets
try:
    # noinspection PyUnresolvedReferences
    from PySide6 import QtMultimedia
except ImportError as ie:
    logging.warning("ImportError for QtMultimedia - maybe because there's no sound card available")
    # -If the system does not have a sound card (as for example Travis CI)
    # -An alternative to this approach is to use this: http://doc.qt.io/qt-5/qaudiodeviceinfo.html#availableDevices
    logging.debug(f"{ie.msg=}")
import matc.constants
import matc.shared
import matc.settings
import matc.gui.breathing_dlg
import matc.gui.settings_dlg
import matc.gui.intro_dlg

SAME_PHRASE = -1


class MainObject(QtCore.QObject):
    """
    We are using this QObject as the core of the application (rather than using a QMainWindow).
    Areas of responsibility:
    * Breathing timer
    * Holds the settings window
    * Holds the breathing-and-rest dialog
    * Handles audio
    * Holds the systray
    """
    def __init__(self):
        super().__init__()
        self.active_phrase_id: int = matc.constants.BREATHING_PHRASE_NOT_SET
        self.circle_count: int = 1

        # System tray and menu setup
        self.tray_icon = QtWidgets.QSystemTrayIcon(self)
        self.tray_icon.activated.connect(self.on_systray_activated)
        self.tray_menu = QtWidgets.QMenu()  # self
        self.tray_menu.aboutToShow.connect(self.on_tray_menu_about_to_show)
        self.tray_open_breathing_dialog_qaction = QtGui.QAction("Breathing Dialog")
        self.tray_menu.addAction(self.tray_open_breathing_dialog_qaction)
        self.tray_open_breathing_dialog_qaction.triggered.connect(self.on_tray_open_breathing_dialog_triggered)
        self.tray_br_phrases_qmenu = QtWidgets.QMenu("Phrases")
        self.tray_br_phrases_qmenu.triggered.connect(self.on_tray_br_phrase_triggered)
        self.tray_menu.addMenu(self.tray_br_phrases_qmenu)
        # self.tray_br_phrases_qmenu.aboutToShow.connect(self.on_phrases_submenu_about_to_show)
        self.tray_open_settings_action = QtGui.QAction("Settings")
        self.tray_menu.addAction(self.tray_open_settings_action)
        self.tray_open_settings_action.triggered.connect(self.on_tray_open_settings_triggered)
        self.tray_notifications_enabled_action = QtGui.QAction("Notifications enabled")
        self.tray_notifications_enabled_action.setCheckable(True)
        self.tray_notifications_enabled_action.setChecked(True)
        self.tray_menu.addAction(self.tray_notifications_enabled_action)

        self.help_menu = self.tray_menu.addMenu("&Help")
        self.show_intro_dialog_action = QtGui.QAction("Show intro wizard", self)
        self.help_menu.addAction(self.show_intro_dialog_action)
        self.show_intro_dialog_action.triggered.connect(self.show_intro_dialog)
        self.about_action = QtGui.QAction("About", self)
        self.help_menu.addAction(self.about_action)
        self.about_action.triggered.connect(self.show_about_box)
        """        
        self.online_help_action = QtGui.QAction("Online help", self)
        self.help_menu.addAction(self.online_help_action)
        self.online_help_action.triggered.connect(self.show_online_help)
        """
        self.feedback_action = QtGui.QAction("Give feedback", self)
        self.help_menu.addAction(self.feedback_action)
        self.feedback_action.triggered.connect(self.show_feedback_dialog)
        self.sysinfo_action = QtGui.QAction("System Information", self)
        self.help_menu.addAction(self.sysinfo_action)
        self.sysinfo_action.triggered.connect(self.show_sysinfo_box)

        self.tray_notifications_enabled_action.toggled.connect(self.on_tray_notifications_enabled_toggled)
        self.tray_quit_action = QtGui.QAction("Quit")
        self.tray_menu.addAction(self.tray_quit_action)
        self.tray_quit_action.triggered.connect(self.on_tray_quit_triggered)
        if sys.flags.dev_mode:
            self.debug_menu = self.tray_menu.addMenu("&Debug")
            self.open_settings_dir_action = QtGui.QAction("Open settings dir", self)
            self.debug_menu.addAction(self.open_settings_dir_action)
            self.open_settings_dir_action.triggered.connect(self.on_open_settings_dir_triggered)
            self.do_manual_gc_action = QtGui.QAction("do manual garbage collection", self)
            self.debug_menu.addAction(self.do_manual_gc_action)
            self.do_manual_gc_action.triggered.connect(self.on_do_manual_gc_triggered)
            self.disable_gc_action = QtGui.QAction("disable garbage collection", self)
            self.debug_menu.addAction(self.disable_gc_action)
            self.disable_gc_action.triggered.connect(self.on_disable_gc_triggered)
        self.tray_menu.setDefaultAction(self.tray_open_breathing_dialog_qaction)



        self.tray_icon.setContextMenu(self.tray_menu)
        self.tray_icon.messageClicked.connect(self.on_tray_icon_message_clicked)
        self.tray_icon.show()

        # Audio setup
        try:
            self.sound_effect = QtMultimedia.QSoundEffect(self)
            # -PLEASE NOTE: A parent has to be given here, otherwise we will not hear anything
        except NameError:
            self.sound_effect = None

        # Timer setup
        self.breathing_reminder_timer = Timer(i_continuous=True)
        self.breathing_reminder_timer.timeout_signal.connect(self.on_breathing_timer_timeout)

        # Window setup
        # ..settings dialog
        self.settings_dlg = matc.gui.settings_dlg.SettingsDlg()
        self.settings_dlg.br_timer_change_signal.connect(self.on_settings_br_timer_change)
        self.settings_dlg.notif_audio_test_signal.connect(self.play_notif_audio)
        self.settings_dlg.br_audio_test_signal.connect(self.play_br_audio)
        # ..breathing dialog
        self.br_dlg = matc.gui.breathing_dlg.BreathingGraphicsView()
        self.br_dlg.first_breathing_gi_signal.connect(self.on_first_breathing_gi)
        self.br_dlg.close_signal.connect(self.on_br_dlg_closed)
        # ..intro dialog
        self.intro_dlg = matc.gui.intro_dlg.IntroDlg()
        self.intro_dlg.close_signal.connect(self.on_intro_dialog_closed)

    def initialize(self, i_is_first_time_started: bool=False):
        # Initialization
        if i_is_first_time_started:
            self.show_intro_dialog()
        self.update_systray_image()
        phrase = matc.settings.get_breathing_phrase(self.active_phrase_id)
        self.br_dlg.show_breathing_dlg(phrase)

    def on_systray_activated(self, i_reason):
        """
        LXDE:
        XFCE: Bug: This function is not called
        MacOS:
        Windows:
        """
        logging.debug("===== on_systray_activated =====")
        logging.debug(f"i_reason = {str(i_reason)} (0->Unknown, 1->Context, 2->DoubleClick, 3->Trigger, 4->MiddleClick) - more info: https://doc.qt.io/qt-6/qsystemtrayicon.html#ActivationReason-enum")
        logging.debug("mouseButtons() = " + str(QtWidgets.QApplication.mouseButtons()))

        """
        self.tray_icon.activated.emit(i_reason)
        if i_reason == QtWidgets.QSystemTrayIcon.Trigger:
            self.restore_window()
        else:
            self.tray_icon.activated.emit(i_reason)
        """
        
        logging.debug("=====")

    def show_intro_dialog(self):
        self.intro_dlg.exec()

    def on_intro_dialog_closed(self):
        self._open_br_dlg()

    def _open_br_dlg(self, i_new_phrase_id: int=SAME_PHRASE):
        if i_new_phrase_id != SAME_PHRASE:
            self.active_phrase_id = i_new_phrase_id
        phrase = matc.settings.get_breathing_phrase(self.active_phrase_id)
        self.br_dlg.show_breathing_dlg(phrase)
        matc.shared.is_breathing_reminder_shown = False
        self.update_systray_image()
        self.breathing_reminder_timer.stop()

    def on_tray_br_phrase_triggered(self, i_action: QtGui.QAction):
        phrase_id_text = i_action.data()
        if phrase_id_text:
            self._open_br_dlg(i_new_phrase_id=int(phrase_id_text))

    def on_tray_quit_triggered(self):
        QtWidgets.QApplication.quit()

    def on_br_dlg_closed(self):
        br_timer_secs: int = matc.settings.settings.get(matc.settings.SK_BREATHING_BREAK_TIMER_SECS)
        self.breathing_reminder_timer.start(br_timer_secs)
        # self.breathing_reminder_timer.start()  # -restarting
        self.update_systray_image()

    def play_audio(self, i_audio_file_path: str, i_volume: int) -> None:
        """
        Please note that the important variable sound_effect is set up at the beginning of the init for settings_dlg.py
        The reason is that to create this variable we need to send a "self" variable (of which type?)
        """
        master_volume: int = matc.settings.settings[matc.settings.SK_MASTER_VOLUME]
        audio_source_qurl = QtCore.QUrl.fromLocalFile(i_audio_file_path)
        self.sound_effect.setSource(audio_source_qurl)
        self.sound_effect.setVolume(float(i_volume / 100) * float(master_volume / 100))
        self.sound_effect.play()

    def on_tray_icon_message_clicked(self):
        """
        Works on: Windows 10
        Doesn't work on: XUbuntu
        https://forum.qt.io/topic/115121/qsystemtrayicon-not-sending-messageclicked-signal-on-linux/6
        """
        print("on_tray_icon_message_clicked")
        self._open_br_dlg()

    def on_breathing_timer_timeout(self):
        if self.br_dlg.isVisible():
            return
        if not self.tray_notifications_enabled_action.isChecked():
            return
        self.update_systray_image(i_is_breathing_reminder_shown=True)
        self.play_notif_audio()
        phrase = matc.settings.get_breathing_phrase(self.active_phrase_id)
        phrase_text: str = f"{phrase.in_breath}\n{phrase.out_breath}"

        self.tray_icon.showMessage(matc.constants.APPLICATION_PRETTY_NAME,
            phrase_text, QtWidgets.QSystemTrayIcon.NoIcon, 5000)
        # -on Windows 10, where the tray icon may normally be hidden, it will be shown during the time that the
        #  notification popup (message) is shown

    def play_notif_audio(self):
        notif_file_path: str = matc.settings.settings[matc.settings.SK_NOTIFICATION_AUDIO_FILE_PATH]
        notif_volume: int = matc.settings.settings[matc.settings.SK_NOTIFICATION_AUDIO_VOLUME]
        self.play_audio(notif_file_path, notif_volume)

    def on_systray_breathing_timeout(self):
        self.update_systray_image()

    def on_first_breathing_gi(self):
        self.play_br_audio()

    def play_br_audio(self):
        br_file_path: str = matc.settings.settings[matc.settings.SK_BREATHING_AUDIO_FILE_PATH]
        br_volume: int = matc.settings.settings[matc.settings.SK_BREATHING_AUDIO_VOLUME]
        self.play_audio(br_file_path, br_volume)

    def on_do_manual_gc_triggered(self):
        # About the Python GC: https://stackify.com/python-garbage-collection/ (see "Generational garbage collection")
        import gc
        logging.debug(f"{gc.get_count()=}")
        logging.debug(f"Triggering manual garbage collection")
        gc.collect()
        logging.debug(f"{gc.get_count()=}")

    def on_disable_gc_triggered(self):
        import gc
        logging.debug(f"{gc.get_threshold()=}")
        logging.debug(f"Disabling the garbage collector by setting threshold to 0,0,0")
        gc.set_threshold(0,0,0)
        logging.debug(f"{gc.get_threshold()=}")

    def show_sysinfo_box(self):
        self._sysinfo_dlg = SysinfoDialog()
        self._sysinfo_dlg.show()

    def show_about_box(self):
        founded_by_str = (
            '<p>Project started and maintained by Tord Dellsén - '
            '<a href="https://sunyatazero.gitlab.io/">Website</a></p>'
        )
        designers_str = (
            '<p>Tord Dellsén - <a href="https://sunyatazero.gitlab.io/">Website</a></p>'
            '<p>Shweta Singh Lodhi - <a href="https://www.linkedin.com/in/lodhishweta">LinkedIn</a></p>'
            '<p>We have also been helped by feedback from our users</p>'
        )
        programmers_str = (
            '<p>Programmers: <a href="https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/graphs/master">'
            'All contributors</a></p>'
        )
        artists_str = (
            '<p>Photography for application icon by Torgny Dellsén - '
            '<a href="https://torgnydellsen.zenfolio.com">torgnydellsen.zenfolio.com</a></p>'
            '<p>Other icons from Open Iconic - MIT license - <a href="https://useiconic.com">useiconic.com</a></p>'
            '<p>Application logo by Yu Zhou (layout modified by Tord Dellsén)'
            '<p>All audio files used have been released into the public domain (CC0)</p>'
        )
        license_str = (
            '<p>Software License: GPLv3</p>'
        )
        # TODO: Link to LICENSE.txt so the user can view it in a popup dialog
        # Please note that this file may be in different places depending on if we have a pip installation or if we are
        # running from a pyinstaller build
        about_html_str = (
            '<html>'
            + founded_by_str
            + designers_str
            + programmers_str
            + artists_str
            + license_str
            + '</html>'
        )

        # noinspection PyCallByClass
        QtWidgets.QMessageBox.about(self, "About Mindfulness at the Computer", about_html_str)

    def on_open_settings_dir_triggered(self):
        config_path: str = matc.shared.get_config_path()
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(config_path))

    def show_feedback_dialog(self):
        feedback_dlg = FeedbackDialog()
        feedback_dlg.exec_()

    def update_systray_image(self, i_is_breathing_reminder_shown: bool=False):
        """
        if i_increment:
            self.circle_count += 1
            if self.circle_count >= 33:
                self.circle_count = 1
        icon_file_name_str = f"{int(self.circle_count)}.png"
        """

        icon_file_name_str = "icon.png"
        # if self.br_and_rest_dlg is not None and self.br_and_rest_dlg.isVisible():
        if i_is_breathing_reminder_shown:
            icon_file_name_str = "icon-b.png"
        systray_icon_path = matc.shared.get_app_icon_path(icon_file_name_str)
        self.tray_icon.setIcon(QtGui.QIcon(systray_icon_path))

    def on_tray_menu_about_to_show(self):
        logging.debug("on_tray_menu_about_to_show")
        # self.rest_progress_qaction.setText("TBD - time since last rest")
        self.tray_br_phrases_qmenu.clear()
        phrases: list[matc.settings.BreathingPhrase] = matc.settings.settings[matc.settings.SK_BREATHING_PHRASES]
        for p in phrases:
            if p.id == matc.constants.BREATHING_PHRASE_NOT_SET:
                logging.error("Breathing phrase not set. This should not be possible")
                phrase_text: str = "nothing"
            else:
                phrase = matc.settings.get_breathing_phrase(p.id)
                phrase_text: str = f"{phrase.in_breath}"
            # qlwi = QtWidgets.QListWidgetItem(phrase_text)
            bp_qaction = QtGui.QAction(phrase_text, parent=self.tray_br_phrases_qmenu)
            # -please note that we have to use the parent param, otherwise the sub-menu will be empty
            bp_qaction.setData(p.id)
            self.tray_br_phrases_qmenu.addAction(bp_qaction)

    def on_tray_open_settings_triggered(self):
        self.settings_dlg.show()
        # self.settings_dlg.showNormal()

    def on_tray_notifications_enabled_toggled(self, i_checked: bool):
        if i_checked:
            self.breathing_reminder_timer.start()
        else:
            self.update_systray_image()

    def on_settings_show_intro_dialog(self):
        self.show_intro_dialog()

    def on_settings_br_timer_change(self):
        br_noftif_time = matc.settings.settings[matc.settings.SK_BREATHING_BREAK_TIMER_SECS]
        self.breathing_reminder_timer.start(br_noftif_time)

    def on_tray_open_breathing_dialog_triggered(self):
        self._open_br_dlg()


class Timer(QtCore.QObject):
    timeout_signal = QtCore.Signal()

    def __init__(self, i_continuous: bool = False):
        super().__init__()
        # self.minutes_elapsed: int = 0
        self.timeout_secs = 0
        self.timer = None
        self.continuous: bool = i_continuous

    def stop(self):
        if self.timer is not None and self.timer.isActive():
            self.timer.stop()
        # self.minutes_elapsed = 0

    def start(self, i_timeout_secs: int = 0):
        self.stop()
        if i_timeout_secs:
            self.timeout_secs = i_timeout_secs
        elif self.timeout_secs:
            pass
        else:
            raise Exception("timeout_secs error, not possible to have 0 for this value")
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.timeout)
        self.timer.start(int(self.timeout_secs * 1000))

    def timeout(self):
        logging.debug("timeout")
        # self.minutes_elapsed += 1
        self.timeout_signal.emit()
        if not self.continuous:
            self.timer.stop()


class SysinfoDialog(QtWidgets.QDialog):
    def __init__(self, i_parent=None):
        super().__init__(i_parent)
        self.setModal(True)

        vbox_l2 = QtWidgets.QVBoxLayout(self)

        self._system_info_str = '\n'.join([
            descr_str + ": " + str(value) for (descr_str, value) in matc.shared.get_system_info()
        ])

        self.system_info_pqte = QtWidgets.QPlainTextEdit()
        vbox_l2.addWidget(self.system_info_pqte)
        self.system_info_pqte.setReadOnly(True)
        self.system_info_pqte.setPlainText(self._system_info_str)

        self.copy_qpb = QtWidgets.QPushButton(self.tr("Copy to clipboard"))
        self.copy_qpb.clicked.connect(self.on_copy_button_clicked)
        vbox_l2.addWidget(self.copy_qpb)

        self.button_box = QtWidgets.QDialogButtonBox(
            QtCore.Qt.Horizontal,
            self
        )
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Close)

        vbox_l2.addWidget(self.button_box)
        self.button_box.rejected.connect(self.reject)
        # -accepted and rejected are "slots" built into Qt

    def on_copy_button_clicked(self):
        qclipboard = QtGui.QGuiApplication.clipboard()
        qclipboard.setText(self._system_info_str)
        # -this will copy the text to the system clipboard


class FeedbackDialog(QtWidgets.QDialog):

    def __init__(self):
        super().__init__()
        self.gui_update_bool = True

        vbox_l2 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l2)

        help_request_str = """<h3>Help Us</h3>
<p>We are grateful for feedback, for example please contact us if you</p>
<ul>
<li>find a bug</li>
<li>have a suggestion for a new feature</li>
<li>have ideas for how to improve the interface</li>
<li>have feedback about what you like about the application and how it helps you when using the computer (we are looking for testimonials!)</li>
</ul>
<p>You can reach us using this email address:</p>"""

        help_request_qll = QtWidgets.QLabel()
        # help_request_qll.setFont(matc.shared.get_font_large())
        help_request_qll.setText(help_request_str)
        help_request_qll.setWordWrap(True)
        vbox_l2.addWidget(help_request_qll)

        email_qll = QtWidgets.QLabel()
        email_qll.setFont(matc.shared.get_font(matc.shared.FontSize.xxlarge))
        email_qll.setText('sunyata.software@gmail.com')
        vbox_l2.addWidget(email_qll)

        emailus_qpb = QtWidgets.QPushButton("Email us!")
        emailus_qpb.clicked.connect(self.on_emailus_clicked)
        vbox_l2.addWidget(emailus_qpb)

        self.show()

    def on_emailus_clicked(self):
        url_string = "mailto:sunyata.software@gmail.com?subject=Feedback"
        webbrowser.open(url_string)
        # Alt: QtGui.QDesktopServices.openUrl(QtCore.QUrl(url_string))

