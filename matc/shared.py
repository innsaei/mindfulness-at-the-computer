import os
import enum
import sys
from string import Template
from PySide6 import QtGui
from PySide6 import QtCore
import matc.constants

"""
This file contains
* enums
* global functions, for example:
  * relating to file names/paths (some using QtCore)
  * relating to font (some using QtGui)
"""

NO_PHRASE_SELECTED_INT = -1

GRID_VERTICAL_SPACING_LINUX = 15
BUTTON_BAR_HORIZONTAL_SPACING_LINUX = 2

APPLICATION_ICON_NAME_STR = "icon.png"
README_FILE_STR = "README.md"

USER_FILES_DIR = "user_files"
IMAGES_DIR = "images"
ICONS_DIR = "icons"
OPEN_ICONIC_ICONS_DIR = "open_iconic"
AUDIO_DIR = "audio"
RES_DIR = "res"

LIGHT_GREEN_COLOR = "#bfef7f"
DARK_GREEN_COLOR = "#7fcc19"
DARKER_GREEN_COLOR = "#548811"
WHITE_COLOR = "#ffffff"
BLACK_COLOR = "#1C1C1C"


class BreathingVisalization(enum.Enum):
    bar = 0
    circle = 1
    line = 2
    columns = 3


class FontSize(enum.Enum):
    # Standard font size is (on almost all systems) 12
    small = 9
    medium = 12
    large = 14
    xlarge = 16
    xxlarge = 24


def get_system_info():
    sys_info_telist=[]
    sys_info_telist.append(("Application name", matc.constants.APPLICATION_NAME))
    sys_info_telist.append(("Application version", matc.constants.APPLICATION_VERSION))
    sys_info_telist.append(("Config path", get_config_path()))
    sys_info_telist.append(("Module path", get_module_path()))
    sys_info_telist.append(("Python version", sys.version))
    sys_info_telist.append(("Qt version", QtCore.qVersion()))
    # sys_info_telist.append(("Pyside version"))
    plugins_path: str = QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.PluginsPath)
    sys_info_telist.append(("Plugins path", plugins_path))
    sys_info = QtCore.QSysInfo()
    sys_info_telist.append(("OS name and version", sys_info.prettyProductName()))
    sys_info_telist.append(
        ("Kernel type and version", sys_info.kernelType() + " " + sys_info.kernelVersion()))
    sys_info_telist.append(("buildCpuArchitecture", sys_info.buildCpuArchitecture()))
    sys_info_telist.append(("currentCpuArchitecture", sys_info.currentCpuArchitecture()))

    system_locale = QtCore.QLocale.system().name()
    sys_info_telist.append(("System Localization", system_locale))


    return sys_info_telist

    # desktop_widget = matc_qapplication.desktop()
    # matc.shared.sys_info_telist.append(("Virtual desktop", str(desktop_widget.isVirtualDesktop())))
    # matc.shared.sys_info_telist.append(("Screen count", str(desktop_widget.screenCount())))
    # matc.shared.sys_info_telist.append(("Primary screen", str(desktop_widget.primaryScreen())))

    """
    # System info (more is available in main.py)
    systray_available_str = "No"
    if self.tray_icon.isSystemTrayAvailable():
        systray_available_str = "Yes"
    sys_info_telist.append(("System tray available", systray_available_str))
    notifications_supported_str = "No"
    if self.tray_icon.supportsMessages():
        notifications_supported_str = "Yes"
    sys_info_telist.append(("System tray notifications supported", notifications_supported_str))
    """


def get_config_path(*args) -> str:
    # application_dir_str = os.path.dirname(os.path.dirname(__file__))
    config_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.ConfigLocation)[0]
    # logging.debug("QStandardPaths.ConfigLocation = " + config_dir)

    # There is a bug in Qt:
    # For Windows, the application name is included in QStandardPaths.ConfigLocation
    # For Linux, it's not included
    if matc.constants.APPLICATION_NAME not in config_dir:
        config_dir = os.path.join(config_dir, matc.constants.APPLICATION_NAME)
    full_path_str = config_dir
    for arg in args:
        full_path_str = os.path.join(full_path_str, arg)
    os.makedirs(os.path.dirname(full_path_str), exist_ok=True)
    return full_path_str


def get_module_path() -> str:
    module_dir_str: str = os.path.dirname(os.path.abspath(__file__))
    # base_dir_str: str = os.path.dirname(module_dir_str)
    # base_dir_str = os.getcwd()
    # -__file__ is the file that was started, in other words mindfulness-at-the-computer.py
    return module_dir_str


def get_audio_path(i_file_name: str = "") -> str:
    ret_user_audio_path_str = os.path.join(get_module_path(), RES_DIR, AUDIO_DIR)
    if i_file_name:
        ret_user_audio_path_str = os.path.join(ret_user_audio_path_str, i_file_name)
    return ret_user_audio_path_str


def get_app_icon_path(i_file_name: str) -> str:
    ret_icon_path_str = os.path.join(get_module_path(), RES_DIR, ICONS_DIR, i_file_name)
    return ret_icon_path_str


def get_icon_path(i_file_name: str) -> str:
    ret_icon_path_str = os.path.join(get_module_path(), RES_DIR, ICONS_DIR, "open_iconic", i_file_name)
    return ret_icon_path_str


def get_font(i_size, i_bold: bool = False) -> QtGui.QFont:
    ret_font = QtGui.QFont()
    ret_font.setPointSize(i_size.value)
    ret_font.setBold(i_bold)
    return ret_font


def get_html(i_text: str, i_focus: bool = False, i_margin: int=0) -> str:
    html_template_base = """<p 
    style="text-align:center;
    padding:0px;margin:${margin}px;font-size:18px;${bold}">
    ${text}
    </p>"""
    html_template = Template(html_template_base)
    bold_html = ""
    if i_focus:
        bold_html = "font-weight:bold;"
    ret_html = html_template.substitute(margin=i_margin, bold=bold_html, text=i_text)
    return ret_html
