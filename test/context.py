# noinspection PyUnresolvedReferences

"""
Please note that this file is needed to ensure that the development version of the application is tested.
If we run tests with ordinary imports we may be using a version of the application which has been installed
with pip

"""

import matc
# noinspection PyUnresolvedReferences
import matc.shared
# noinspection PyUnresolvedReferences
import matc.main_object
# noinspection PyUnresolvedReferences
import matc.settings

"""
This code is no longer needed:

import os
import sys

this_dir: str = os.path.dirname(__file__)
project_dir: str = os.path.abspath(os.path.join(this_dir, ".."))
# sys.path.insert(0, project_dir)
package_dir: str = os.path.join(project_dir, "mindfulness-at-the-computer")
print(f"Inserting package directory: {package_dir=}")
sys.path.insert(0, package_dir)
print(f"{sys.path=}")
"""
