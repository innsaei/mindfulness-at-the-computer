import logging
import sys
import time
import pytest
"""
Pytest plugins:
* pytest-qt - gives the qtbot fixture
  * pytest-qt does not have to be imported
  * qtbot takes care of creating a QApplication (which otherwise we'd have to create ourselves,
    `test_app = QtWidgets.QApplication(sys.argv)`)
  * wraps QtTest/QTest??? (so we don't need `from PySide6.QtTest import QTest`)

QTest Documentation:
* https://doc.qt.io/qtforpython/PySide6/QtTest/QTest.html
* See also this link https://www.riverbankcomputing.com/static/Docs/PyQt6/api/qttest/qtest.html which includes important
functions like mouseClick (which at the time of writing is not included in the previous link above)


## Pytest: Some failing assertions gives segfault

* This happens only in certain places, but is reproducible: br_dlg, but not intro_dlg
* It happens only when running from terminal, not when running individual tests (using pycharms pytest config)

```
(venv) sunyata@sunyata-HP-Laptop:~/PycharmProjects/mindfulness-at-the-computer$ pytest -rA -vv test/
================================ test session starts ================================
platform linux -- Python 3.10.4, pytest-7.1.2, pluggy-1.0.0 -- /home/sunyata/PycharmProjects/mindfulness-at-the-computer/venv/bin/python
cachedir: .pytest_cache
PySide6 6.3.0 -- Qt runtime 6.3.0 -- Qt compiled 6.3.0
rootdir: /home/sunyata/PycharmProjects/mindfulness-at-the-computer
plugins: qt-4.0.2
collected 3 items                                                                   

test/test_main.py::test_success PASSED                                        [ 33%]
test/test_main.py::test_br_dlg FAILED                                         [ 66%]
test/test_main.py::test_intro_dlg PASSED                                      [100%]

===================================== FAILURES ======================================
____________________________________ test_br_dlg ____________________________________

qtbot = <pytestqt.qtbot.QtBot object at 0x7f4543406470>
main_object = <matc.main_object.MainObject(0x55a32aef7620) at 0x7f4541a0b780>

Fatal Python error: Segmentation fault

Current thread 0x00007f454b224000 (most recent call first):
  Garbage-collecting
[... Since this is a segfault issue the top line (in other words the last instructions run) here can be different and isn't relevant]
  File "/home/sunyata/PycharmProjects/mindfulness-at-the-computer/venv/lib/python3.10/site-packages/_pytest/config/__init__.py", line 187 in console_main
  File "/home/sunyata/PycharmProjects/mindfulness-at-the-computer/venv/bin/pytest", line 8 in <module>

Extension modules: xxsubtype, shiboken6.Shiboken, PySide6.QtCore, PySide6.QtGui, PySide6.QtWidgets, PySide6.QtTest, PySide6.QtNetwork, PySide6.QtMultimedia (total: 8)
Segmentation fault (core dumped)
(venv) sunyata@sunyata-HP-Laptop:~/PycharmProjects/mindfulness-at-the-computer$ 
```


"""
from PySide6 import QtCore
from .context import matc

# https://doc.qt.io/qt-5/qpluginloader.html
# https://doc.qt.io/qt-5/qlibraryinfo.html
plugins_path: str = QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.PluginsPath)
print(f"{plugins_path=}")


@pytest.fixture
def main_object():
    ret_main_object = matc.main_object.MainObject()
    return ret_main_object


def test_success():
    assert 4 == 4


# TODO: Stess test, segfault
# TODO: backend tests
# TODO: integration
# TODO: timer tests


def test_settings_dlg(qtbot, main_object):
    settings_dialog=main_object.settings_dlg
    assert settings_dialog.isVisible()==False
    main_object.on_tray_open_settings_triggered()
    assert settings_dialog.isVisible()==True

    assert settings_dialog.move_mouse_cursor_qcb.isChecked() ==False
    qtbot.mouseClick(settings_dialog.move_mouse_cursor_qcb, QtCore.Qt.LeftButton)
    assert settings_dialog.move_mouse_cursor_qcb.isChecked() ==True

    # Unsure why qtbot.mouseClick doesn't work (maybe because we are using a composite widget?)
    # qtbot.mouseClick(settings_dialog.columns_bv_option_cw.br_vis_qrb, QtCore.Qt.LeftButton, delay=1000)
    settings_dialog.columns_bv_option_cw.br_vis_qrb.click()
    assert compare_with_br_vis_setting(matc.shared.BreathingVisalization.columns)

    settings_dialog.line_bv_option_cw.br_vis_qrb.click()
    assert compare_with_br_vis_setting(matc.shared.BreathingVisalization.line)

    settings_dialog.bar_bv_option_cw.br_vis_qrb.click()
    assert compare_with_br_vis_setting(matc.shared.BreathingVisalization.bar)

    settings_dialog.circle_bv_option_cw.br_vis_qrb.click()
    assert compare_with_br_vis_setting(matc.shared.BreathingVisalization.circle)


def compare_with_br_vis_setting(i_br_vis: matc.shared.BreathingVisalization.columns) -> bool:
    vis_nr = matc.settings.settings[matc.settings.SK_BREATHING_VISUALIZATION]
    return vis_nr == i_br_vis.value


def compare_with_setting():
    pass


@pytest.mark.skip
def test_systray_icon(qtbot, main_object):
    systray_icon=main_object.tray_icon


def test_systray_menu(qtbot, main_object):
    systray_menu=main_object.tray_menu
    action_list=systray_menu.actions()
    # print(f"{action_list=}")
    assert main_object.tray_open_breathing_dialog_qaction in action_list

    settings_dialog=main_object.settings_dlg
    assert settings_dialog.isVisible()==False
    main_object.tray_open_settings_action.trigger()
    assert settings_dialog.isVisible()==True


def test_br_dlg(qtbot, main_object):
    """
    TODO: How to test interactions with the graphicsitems/graphicsobjects?
    * coords?
    * programmatically starting the hovering?
    """
    br_dlg = main_object.br_dlg
    assert br_dlg.isVisible() == False
    main_object.initialize()
    assert br_dlg.isVisible() == True


def test_intro_dlg(qtbot, main_object):
    """
    Difficult to test visibility since the intro dialog uses .exec() (rather than .show()) to start
    """
    intro_dlg = main_object.intro_dlg
    i = 0
    assert intro_dlg.wizard_qsw_w3.currentIndex() == i
    while i < intro_dlg.wizard_qsw_w3.count() - 1:
        qtbot.mouseClick(intro_dlg.next_qpb, QtCore.Qt.LeftButton)
        i += 1
        assert intro_dlg.wizard_qsw_w3.currentIndex() == i
    while i > 0:
        qtbot.mouseClick(intro_dlg.prev_qpb, QtCore.Qt.LeftButton)
        i -= 1
        assert intro_dlg.wizard_qsw_w3.currentIndex() == i



"""
# import unittest


class MainTest(unittest.TestCase):
    # "@unittest.skip" can be used to skip a test

    @classmethod
    def setUpClass(cls):
        matc.shared.testing_bool = True

    def setUp(self):
        pass

    def test_main_object(self):
        main_object = matc.main_object.MainObject()
        # sys.exit(test_app.exec())
        time.sleep(5)


if __name__ == "__main__":
    unittest.main()
"""